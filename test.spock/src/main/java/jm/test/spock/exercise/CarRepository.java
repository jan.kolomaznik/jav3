package jm.test.spock.exercise;

import java.util.Optional;

public interface CarRepository {

    Optional<Car> save(Car car);

    Optional<Car> findById(long carId);

    Iterable<Car> findAllCars();
}
