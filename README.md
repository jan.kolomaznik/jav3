JAVA - POKROČILÉ TECHNIKY PROGRAMOVÁNÍ  
====================================== 

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 
 
Tento seminář je určen převážně méně zkušeným vývojářům, ačkoliv coby souhrnné a ucelené zopakování jej mohou ocenit i zkušenější uživatelé jazyka Java. 

Osnova kurzu 
------------ 

- **Kuchařka designu a vytváření kódu**
  - DRW, DRY a další principy
  - Píšeme metodu: od jména k příkazům
  - Ortogonalita metod
  - Overloading, overriding
  - Zpracování parametrů
  - Používání varargs
  - Styly psaní kódu, idiomy a vzory
  - Testování, ladění, optimalizace

- **Typová teorie, kontrakt**
  - Terminologie: abstrakce, rozhraní, kontrakt, typ, třída, instance
  - Liskov Substitution Principle, odvozené a příbuzné principy, subtyping a subclassing
  - Důsledky pro návrh tříd, viditelnost částí třídy
  - Dědičnost: kdy a proč, obvyklé chyby
  - Dědičnost a kompozice, používání rozhraní
  - Idiomy, techniky, postupy

- **Vytváření instancí**
  - Jak je možné vytvořit instanci
  - Použitelné návrhové vzory a jejich porovnání
  - Obvyklé chyby, postupy

- **Používání výjimek**
  - Teorie a realita
  - Časté chyby a zneužívání výjimek
  - Výjimky: checked vs. unchecked; návrh a kontrakt
  - Používání výjimek, failure atomicity
  
- **Užitečné nástroje**
  - Enum: podceňovaný a špatně využívaný
  - Používání příkazu switch
  - Vnitřní třídy
  - Základní metody, třídy a rozhraní

- **Modifikovatelnost**
  - Úskalí modifikovatelnosti, výhody nemodifikovatelnosti
  - „Nerozbitné“ třídy, pravá a efektivní nemodifikovatelnost
  - Získání instance, použitelné návrhové vzory
  - Nevýhody
 

