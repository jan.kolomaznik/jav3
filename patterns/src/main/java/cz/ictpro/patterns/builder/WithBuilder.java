package cz.ictpro.patterns.builder;

import java.time.LocalDate;
import java.time.Month;

public class WithBuilder {

    public static void main(String[] args) {
        System.out.println(Person.builder()
                .name("Tomas")
                .surname("Novak")
                .birth(LocalDate.of(1999, Month.JANUARY, 15))
        );
    }
}
