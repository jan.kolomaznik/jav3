package cz.ictpro.patterns.builder;

import javax.sound.midi.Soundbank;

public class NoBuilder {

    public static void main(String[] args) {
        Person pepa = new Person();
        pepa.setName("Pepa");
        pepa.setWeight(120.5);
        pepa.setNickname("Kulička");
        System.out.println(pepa);

        Person tomas = new Person("Tomas", null, null, "Tyčka", 60.0, 215.0);
        System.out.println(tomas);
    }

}
