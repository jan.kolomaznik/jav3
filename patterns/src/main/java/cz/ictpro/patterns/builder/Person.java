package cz.ictpro.patterns.builder;

import java.time.LocalDate;

public class Person {

    public static class Builder {

        private Person person = new Person();

        public Builder name(String name) {
            person.name = name;
            return this;
        }
        public Builder surname(String surname) {
            person.surname = surname;
            return this;
        }
        public Builder birth(LocalDate birth) {
            person.birth = birth;
            return this;
        }
        public Builder nickname(String nickname) {
            person.nickname = nickname;
            return this;
        }
        public Builder weight(Double weight) {
            person.weight = weight;
            return this;
        }
        public Builder height(Double height) {
            person.height = height;
            return this;
        }
        public Person build() {
            return person;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private String name;
    private String surname;
    private LocalDate birth;
    private String nickname;
    private Double weight;
    private Double height;

    public Person() {
    }

    public Person(String name, String surname, LocalDate birth, String nickname, Double weight, Double height) {
        this.name = name;
        this.surname = surname;
        this.birth = birth;
        this.nickname = nickname;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birth=" + birth +
                ", nickname='" + nickname + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                '}';
    }
}
