package cz.ictpro.patterns.servant;

public class Car implements Inspectionable {

    private final String spz;

    private final InspectionServant inspectionServant;

    public Car(String spz) {
        this.spz = spz;
        this.inspectionServant = new InspectionServant(this);
    }

    @Override
    public String getId() {
        return "Car: " + spz;
    }

    public String doSTK(int id) {
        return inspectionServant.doInspection(id);
    }
}
