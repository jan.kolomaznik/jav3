package cz.ictpro.patterns.servant;

public interface Inspectionable {

    String getId();
}
