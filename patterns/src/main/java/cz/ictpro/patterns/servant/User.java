package cz.ictpro.patterns.servant;

public class User {

    public static void main(String[] args) {
        Car car = new Car("ABC 1234");
        Person person = new Person("Pepa", "Novak");

        System.out.println(car.doSTK(123));
        System.out.println(person.doInspection(124));
    }


}
