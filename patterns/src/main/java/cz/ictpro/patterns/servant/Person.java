package cz.ictpro.patterns.servant;

public class Person implements Inspectionable {

    private final String name;

    private final String surname;

    private final InspectionServant inspectionServant;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.inspectionServant = new InspectionServant(this);
    }

    @Override
    public String getId() {
        return String.format("Person %s %s", name, surname);
    }

    public String doInspection(int id) {
        return inspectionServant.doInspection(id);
    }
}
