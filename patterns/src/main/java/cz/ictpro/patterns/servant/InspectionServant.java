package cz.ictpro.patterns.servant;

public class InspectionServant {

    private final Inspectionable inspectionable;

    public InspectionServant(Inspectionable inspectionable) {
        this.inspectionable = inspectionable;
    }

    public String doInspection(int id) {
        return String.format("Inspection %d: %s\n", id, inspectionable.getId());
    }

}
