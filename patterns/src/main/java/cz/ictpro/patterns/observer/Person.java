package cz.ictpro.patterns.observer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Person {

    private String name;

    private String surname;

    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        String oldName = name;
        name = newName;
        pcs.firePropertyChange("name", oldName, newName);
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String newSurname) {
        String oldSurname = surname;
        surname = newSurname;
        pcs.firePropertyChange("surname", oldSurname, newSurname);
    }
}
