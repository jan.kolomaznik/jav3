package cz.ictpro.patterns.observer;

public class Main {

    public static void main(String[] args) {
        Person person = new Person("Pepa", "Novak");
        PersonObserver observer = new PersonObserver(person);
        person.setName("Honza");
    }
}
