package cz.ictpro.patterns.observer;

import java.util.EventObject;

public class PersonObserver {

    private Person person;

    public PersonObserver(Person person) {
        this.person = person;
        this.person.addPropertyChangeListener(this::onChange);
    }

    public void onChange(EventObject eventObject){
        System.out.println(eventObject);
    }
}
