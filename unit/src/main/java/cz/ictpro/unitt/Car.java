package cz.ictpro.unitt;

public class Car {

    private final String spz;

    public Car(String spz) {
        this.spz = spz;
    }

    @Override
    public String toString() {
        return "Car{" +
                "spz='" + spz + '\'' +
                '}';
    }
}
