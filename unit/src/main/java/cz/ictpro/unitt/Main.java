package cz.ictpro.unitt;

public class Main {

    public static void main(String[] args) {
        Car car = new Car("ABC 1234");
        Driver driver = new Driver("Pepa");
        RentService rentService = new RentService(null);
        rentService.rentCarToDriver(car, driver);
        System.out.println(driver);
    }
}
