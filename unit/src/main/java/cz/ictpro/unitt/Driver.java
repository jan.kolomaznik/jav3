package cz.ictpro.unitt;

public class Driver {

    private final String name;

    private Car drive;

    public Driver(String name) {
        this.name = name;
    }

    public void setDrive(Car drive) {
        this.drive = drive;
    }


    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", drive=" + drive +
                '}';
    }
}
