package cz.ictpro.unitt;

public interface DriverRepository {

    public Boolean saveDriver(Driver driver);
}
