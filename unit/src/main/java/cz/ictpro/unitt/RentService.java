package cz.ictpro.unitt;

public class RentService {

    private DriverRepository driverRepository;

    public RentService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public Boolean rentCarToDriver(Car car, Driver driver) {
        // if (driver.)
        driver.setDrive(car);
        return driverRepository.saveDriver(driver);
    }
}
