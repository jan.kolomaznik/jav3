package cz.ictpro.unitt;

import spock.lang.Specification;

import static org.junit.Assert.*;

public class RentServiceTest extends Specification {

    private RentService rentService;
    private DriverRepository driverRepository;


    public void setup() {
        driverRepository = Mock(DriverRepository);
        rentService = new RentService(driverRepository);
    }

    def "Rent car basic test."() {
        given: // setup
        Car car = new Car("ABC 1234");
        Driver driver = new Driver("Pepa");
        1 * driverRepository.saveDriver(driver) >> true

        when: // call method
        Boolean result =  rentService.rentCarToDriver(car, driver);

        then: // asserts
        result == true

        assert driver.drive == car;
    }

    public void "Rent car to driver, when car is null."() {
        given: // setup
        Driver driver = new Driver("Pepa");

        when: // call method
        rentService.rentCarToDriver(null, driver);

        then: // asserts
        assert driver.drive == null;
    }

}