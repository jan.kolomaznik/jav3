package sample;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {

    @FXML
    private Button button;

    @FXML
    private TextField input;

    @FXML
    private Label output;

    @FXML
    private ListView<String> list;

    @FXML
    private CheckBox enabled;

    @FXML
    public void initialize() {
        button.disableProperty().bindBidirectional(enabled.selectedProperty());
    }

    public void click(Event event) {
        System.out.println(event);
        String msg = input.getText();
        input.clear();
        output.setText(msg);
        list.getItems().add(msg);
    }
}
