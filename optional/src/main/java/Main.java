import java.util.Optional;

public class Main {

    public static void main(String[] args) {

        // Definice
        Optional<String> optionalEmpty = Optional.empty();
        Optional<String> optionalHello = Optional.of("Hello");

        // Primy pristup
        System.out.println(optionalHello.get());
        //System.out.println(optionalEmpty.get());

        // Test exitence
        System.out.println(optionalEmpty.isEmpty());
        System.out.println(optionalHello.isPresent());


        System.out.println(ifNullThrowException(optionalEmpty));
        System.out.println(ifNullThrowException(optionalHello));

        System.out.println(optionalEmpty.orElse("Is Empty"));
        System.out.println(optionalHello.orElse("Is Empty"));

        System.out.println(optionalEmpty.or(Main::supplier));
        System.out.println(optionalHello.or(Main::supplier));

        System.out.println(optionalEmpty.map(String::toUpperCase));
        System.out.println(optionalHello.map(String::toUpperCase));
    }

    public static Optional<String> supplier() {
        return Optional.of("From code");
    }

    public static String ifNullThrowException(Optional<String> optional) {
        try {
            return optional.orElseThrow(IllegalArgumentException::new);

        } catch (IllegalArgumentException e) {
            return e.toString();
        }
    }


}
