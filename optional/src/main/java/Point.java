import java.util.Objects;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point {

    private static final double DIMENSION = 2;

    public static final Point NOT_DEFINED = new Point(Double.NaN, Double.NaN) {

        @Override
        public double distance(Point other) {
            return Double.NaN;
        }

    };

    Double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point other) {
        if (other == NOT_DEFINED) {
            return Double.NaN;
        }

        return sqrt(pow(this.x - other.x, DIMENSION) +
                    pow(this.y - other.y, DIMENSION)
        );
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static void main(String[] args) {
        Point a = new Point(1,2);
        Point b = new Point(-1,-2);
        System.out.println(a.distance(b));

        System.out.println(a.distance(NOT_DEFINED));
        System.out.println(NOT_DEFINED.distance(a));


        if (Objects.equals(a, b)) {

        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Objects.equals(x, point.x) &&
                Objects.equals(y, point.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
